<?php
/**
 * The template for displaying all single sport
 *
 * @package    WordPress
 * @subpackage Custom_Theme
 * @since      1.0
 * @version    1.0
 */

get_header();
get_template_part( 'template-parts/content', 'banner' ); ?>
	<div class="article">
		<div class="article__content article__content--sports container">
			<h1><?php the_title(); ?></h1>
			<?php
			// Start the loop.
			while ( have_posts() ) {
				the_post();

				// Include the single post content template.
				get_template_part( 'template-parts/content', get_post_type() );
			} ?>
		</div>
	</div>
<?php get_template_part( 'template-parts/content', 'trial' );
get_template_part( 'template-parts/content', 'related-news' );
get_template_part( 'template-parts/content', 'related-sport' );
get_footer();
