/**
 * Scroll To Fixed
 *
 * This plugin is used to fix elements on the page (top, bottom, anywhere); however.
 */

/* jshint ignore:start */
// =require ../../../../../../node_modules/scrolltofixed/jquery-scrolltofixed.js
/* jshint ignore:end */
