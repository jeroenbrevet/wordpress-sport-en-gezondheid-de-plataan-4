<?php
/**
 * The template part for displaying banner on website
 *
 * @package    WordPress
 * @subpackage Custom_Theme
 * @since      3.4.6
 * @version    3.4.6
 */

$fallback = get_field( 'banner_image', 'option' );
if ( is_search() || is_404() ) {

	$url = $fallback;
} elseif ( is_home() ) {
	$url = get_field( 'banner_image', get_option( 'page_for_posts' ) );
} elseif ( is_singular( 'post' ) ) {

	if ( ( $post_banner = get_field( 'banner_image' ) ) && ! empty( $post_banner ) ) {
		$url = $post_banner;
	} else {
		$url = get_field( 'banner_image', get_option( 'page_for_posts' ) );
	}
} elseif ( is_singular( 'sport' ) ) {

	if ( ( $post_banner = get_field( 'banner_image' ) ) && ! empty( $post_banner ) ) {
		$url = $post_banner;
	} else {
		$url = get_field( 'banner_image', get_option( 'page_for_posts' ) );
	}
} elseif ( is_archive() || is_tax( 'sport_cat' ) ) {

	$archive = get_archive();
	$url     = get_field( 'banner_image', $archive->ID );
} else {
	$url = get_field( 'banner_image' );
}

if ( empty( $url ) ) {
	$url = $fallback;
}

if ( ! empty( $url ) ): ?>
	<div class="banner" style="background-image: url(<?php echo $url['sizes']['banner']; ?>);">
		<?php if ( function_exists( 'yoast_breadcrumb' ) ): ?>
			<div class="container">
				<?php yoast_breadcrumb( '<p class="banner__breadcrumbs">', '</p>' ); ?>
			</div>
		<?php endif; ?>
	</div>
<?php endif; ?>