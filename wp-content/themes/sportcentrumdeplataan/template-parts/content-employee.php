<?php
/**
 * The template part for displaying employee content
 *
 * @package    WordPress
 * @subpackage Custom_Theme
 * @since      3.4.6
 * @version    3.4.6
 */

$emp = [
	'post_type'      => 'medewerker',
	'posts_per_page' => -1,
	'tax_query'      => []
];

if ( isset( $_GET['q'] ) && ! empty( $_GET['q'] ) ) {
	$cate_id = esc_attr( $_GET['q'] );

	$emp['tax_query'] = [
		[
			'taxonomy' => 'medewerker_cat',
			'field'    => 'term_id',
			'terms'    => $cate_id,
		]
	];
}

$args = [
	'orderby'    => 'id',
	'order'      => 'asc',
	'hide_empty' => false,
];

$employee = new WP_Query( $emp );
$terms    = get_terms( 'medewerker_cat', $args );

if ( $employee->have_posts() || ! empty( $terms ) ): ?>
	<div class="employee container">
		<?php if ( ! empty( $terms ) ): ?>
			<ul class="employee__filter row d-flex justify-content-center">
				<?php foreach ( $terms as $term ): ?>
					<li class="col-xl-2 col-lg-3 col-sm-4 <?php echo ( esc_attr( $_GET['q'] == $term->term_id ) ) ? 'is-active' : ''; ?>">
						<a href="<?php echo get_permalink(); ?>?q=<?php echo $term->term_id; ?>"><?php echo $term->name; ?></a>
					</li>
				<?php endforeach; ?>
                <li class="col-xl-2 col-lg-3 col-sm-4 <?php echo ( ! isset( $_GET['q'] ) ) ? 'is-active' : ''; ?>">
                    <a href="<?php the_permalink(); ?>"><?php _t( 'Alle medewerkers' ); ?></a>
                </li>
			</ul>
		<?php endif;

		if ( $employee->have_posts() ): $i = 1; ?>
			<ul class="employee__list row">
				<?php while ( $employee->have_posts() ): $employee->the_post();
				if($i==7){ ?>
						<li class="employee__quote"><?php the_field('worker_quotes','option'); ?></li>
						<?php } ?>
						<li class="col-lg-2 col-sm-4">
						 <?php
                                        while(have_rows('timetable_lessons')) : the_row();

                                        $instructor     = get_sub_field('timetable_lesson_instructor');
                                    ?>
							<div id="<?php echo $instructor;?>" class="employee__block">
								<?php the_post_thumbnail( 'employee' ); ?>

								<div class="is-overlay">
									<h4><?php the_title(); ?></h4>

									<?php if ( ( $content = get_field( 'overlay_content' ) ) && ! empty( $content ) ): ?>
										<section>
											<?php echo $content; ?>
										</section>
									<?php endif; ?>
								</div>
							</div>
						</li>
					<?php  $i++; endwhile;
				wp_reset_postdata(); ?>
			</ul>
		<?php endif; ?>
	</div>
<?php endif; ?>