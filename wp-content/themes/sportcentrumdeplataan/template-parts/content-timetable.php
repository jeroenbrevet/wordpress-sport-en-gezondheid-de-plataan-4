<?php
/**
 * The template part for displaying contact content
 *
 * @package    WordPress
 * @subpackage Custom_Theme
 * @since      3.4.6
 * @version    3.4.6
 */

?>
<div class="timetable">
    <div class="container">
        <?php
            if(have_rows('timetable_settings')) :
                while(have_rows('timetable_settings')) : the_row();

                    $days = get_sub_field('timetable_day');
        ?>
                    <div class="row timetable__row-bg-pattern">
                        <div class="col-sm-12 timetable__day-heading">
                            <div class="timetable__title"><h3><?php echo $days;?></h3></div>
                        </div>
                        <?php
                            if(have_rows('timetable_lessons')) :
                        ?>
                            <div class="col-sm-12 timetable__table-res">
                                <div class="timetable__content">
                                    <div class="timetable__heading">
                                        <div class="timetable__cell"><p><?php _t( 'Les' ); ?></p></div>
                                        <div class="timetable__cell"><p><?php _t( 'Van' ); ?></p></div>
                                        <div class="timetable__cell"><p><?php _t( 'Tot' ); ?></p></div>
                                        <div class="timetable__cell"><p><?php _t( 'Zaal' ); ?></p></div>
                                        <div class="timetable__cell"><p><?php _t( 'Instructeur' ); ?></p></div>
                                        <div class="timetable__cell"><p><?php _t( 'Opmerking' ); ?></p></div>
                                    </div>
                                    <?php
                                        while(have_rows('timetable_lessons')) : the_row();

                                        $lessonName     = get_sub_field('timetable_lesson_name');
                                        $lessonLink     = get_sub_field('timetable_lesson_link');
                                        $lessonStart    = get_sub_field('timetable_lesson_start');
                                        $lessonEnd      = get_sub_field('timetable_lesson_end');
                                        $lessonRoom     = get_sub_field('timetable_lesson_room');
                                        $instructor     = get_sub_field('timetable_lesson_instructor');
                                        $instructorLink = get_sub_field('timetable_lesson_instructor_link');
                                        $lessonRemark   = get_sub_field('timetable_lesson_remark');

                                    ?>
                                          <div class="timetable__row">
                                              <div class="timetable__cell"><p><a href="<?php echo $lessonLink;?>"><?php echo $lessonName;?></a></p></div>
                                              <div class="timetable__cell"><p><?php echo $lessonStart;?></p></div>
                                              <div class="timetable__cell"><p><?php echo $lessonEnd;?></p></div>
                                              <div class="timetable__cell"><p><?php echo $lessonRoom;?></p></div>
                                              <div class="timetable__cell"><p><a href="/medewerkers/#<?php echo $instructor;?>"><?php echo $instructor;?></a></p></div>
                                              <div class="timetable__cell"><p><?php echo $lessonRemark;?></p></div>
                                          </div>
                                    <?php
                                        endwhile;
                                    ?>
                                </div>
                            </div>
                        <?php
                            endif;
                        ?>
                    </div>
        <?php
                endwhile;
            endif;
        ?>
    </div>
</div>
