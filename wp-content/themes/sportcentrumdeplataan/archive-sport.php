<?php
/**
 * The template for displaying archive pages
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * If you'd like to further customize these archive views, you may create a
 * new template file for each one. For example, tag.php (Tag archives),
 * category.php (Category archives), author.php (Author archives), etc.
 *
 * @link       https://codex.wordpress.org/Template_Hierarchy
 *
 * @package    WordPress
 * @subpackage Custom_Theme
 * @since      1.0
 * @version    1.0
 */
global $wp_query;

$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged', 1 ) : 1;
$next  = ( ( $paged + 1 ) <= $wp_query->max_num_pages ) ? $paged + 1 : false;

if ( is_ajax_call() ) {
	while ( have_posts() ) {
		the_post();

		get_template_part( 'template-parts/loop', 'sport' );
	}

	if ( $next ) {
		echo '<div class="sports__loader">';
		echo '<a class="button js-loader" href="' . esc_url( add_query_arg( [
				'paged'   => $next,
				'caching' => 'false'
			] ) ) . '">' . __t( 'Meer Inladen' ) . '</a>';
		echo '</div>';
	}

	exit();
}

$archive = get_archive();

get_header();
get_template_part( 'template-parts/content', 'banner' ); ?>
	<div class="article">
		<div class="article__content container">
			<h1><?php echo $archive->post_title; ?></h1>

			<?php echo apply_filters( 'the_content', $archive->post_content ); ?>
		</div>
	</div>

	<div class="sports container">
		<?php get_template_part( 'template-parts/sport', 'category' ); ?>

		<div class="sports__list" id="ajax-holder" data-component="ajax-load">
			<ul class="row d-flex justify-content-center">
				<?php
				if ( have_posts() ) {
					// Start the loop.
					while ( have_posts() ) {
						the_post();

						/**
						 * Include the Post-Format-specific template for the content.
						 * If you want to override this in a child theme, then include a file
						 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
						 */
						get_template_part( 'template-parts/loop', 'sport' );
					}
				} else {
					// If no content, include the "No posts found" template.
					get_template_part( 'template-parts/content', 'none' );
				} ?>
			</ul>

			<?php if ( $next ) : ?>
				<div class="sports__loader">
					<a class="button js-loader" href="<?php echo esc_url( add_query_arg( [
						'paged'   => $next,
						'caching' => 'false'
					] ) ); ?>"><?php _t( 'Meer Inladen' ); ?></a>
				</div>
			<?php endif; ?>
		</div>
	</div>
<?php
get_template_part( 'template-parts/content', 'latest' );
get_footer();
