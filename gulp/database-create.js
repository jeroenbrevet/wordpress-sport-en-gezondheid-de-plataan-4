module.exports = function( gulp, plugins, paths, config ) {
	return function() {
		var db = require( 'gulp-db' )(
			{
				user: config.database.user,
				password: config.database.password,
				host: config.database.hostname
			}
		);

		db.create( config.database.name )
	};
};
