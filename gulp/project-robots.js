module.exports = function( gulp, plugins ) {
	return function() {
		if ( ! plugins.fileExists( './robots.txt' ) ) {
			var content = 'User-agent: *' + '\n' +
			              'Disallow:';

			return gulp.src( './*' )
			           .pipe( plugins.file( 'robots.txt', content ) )
			           .pipe( gulp.dest( './' ) );
		}
	};
};
