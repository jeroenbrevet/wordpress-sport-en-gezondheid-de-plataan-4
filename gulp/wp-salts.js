var request = require( 'request' );

module.exports = function( gulp, plugins ) {
	return function() {
		if ( ! plugins.fileExists( './wp-salts.php' ) ) {
			var content = '<?php' + '\n' +
			              '/**' + '\n' +
			              ' * Authentication Unique Keys and Salts.' + '\n' +
			              ' *' + '\n' +
			              ' * Change these to different unique phrases!' + '\n' +
			              ' * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}' + '\n' +
			              ' * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.' + '\n' +
			              ' *' + '\n' +
			              ' * @since 2.6.0' + '\n' +
			              ' */' + '\n\n';

			request(
				'https://api.wordpress.org/secret-key/1.1/salt/', function( error, response, body ) {

					if ( ! error && response.statusCode == 200 ) {

						return gulp.src( './*' )
						           .pipe( plugins.file( 'wp-salts.php', content + body ) )
						           .pipe( gulp.dest( './' ) );

					}

				}
			);
		}
	};
};
