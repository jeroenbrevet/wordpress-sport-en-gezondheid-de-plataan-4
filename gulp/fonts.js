module.exports = function( gulp, plugins, paths ) {
	return function() {
		// Which browsers should be prefixed
		var browsers = [
			'ie >= 10',
			'ie_mob >= 10',
			'ff >= 30',
			'chrome >= 34',
			'safari >= 7',
			'opera >= 23',
			'ios >= 7',
			'android >= 4.4',
			'bb >= 10'
		];

		var fonts = gulp.src( paths.src.fonts + '/**/*.{eot,svg,ttf,woff,woff2,otf}' )
		                .pipe( plugins.changed( paths.dist.fonts ) )
		                .pipe( gulp.dest( paths.dist.fonts ) );

		var scss = gulp.src( paths.src.fonts + '/**/*.{css,scss}' )
		               .pipe(
			               plugins.sass(
				               {
					               outputStyle: 'compressed',
					               precision: 10
				               }
			               ).on( 'error', plugins.sass.logError )
		               )
		               .pipe( plugins.autoprefixer( browsers ) )
		               .pipe( plugins.size( { title: 'Fonts: Theme' } ) )
		               .pipe( plugins.changed( paths.dist.fonts ) )
		               .pipe( plugins.cssnano() )
		               .pipe( gulp.dest( paths.dist.fonts ) );

		return plugins.merge( fonts, scss );
	};
};
