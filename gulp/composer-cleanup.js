module.exports = function( gulp, plugins, paths ) {
	return function() {
		return gulp.src( './composer.json' )
		           .pipe(
			           plugins.jsonEditor(
				           function( json ) {

					           var requireDev = json[ 'require-dev' ];

					           if ( 'db-theme/customtheme' in requireDev ) {

						           delete requireDev[ 'db-theme/customtheme' ];

					           }

					           json[ 'require-dev' ] = requireDev;

					           return json;
				           },
				           {
					           'indent_char': '\t',
					           'indent_size': 1
				           }
			           )
		           )
		           .pipe( gulp.dest( './' ) );
	};
};
