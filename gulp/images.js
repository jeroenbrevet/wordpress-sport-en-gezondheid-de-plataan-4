module.exports = function( gulp, plugins, paths ) {
	return function() {
		return gulp.src( paths.src.images + '/**/*.{png,jpg,jpeg,gif,svg}' )
		           .pipe( plugins.changed( paths.dist.images ) )
		           .pipe(
			           plugins.imagemin(
				           {
					           progressive: true,
					           interlaced: true
				           }
			           )
		           )
		           .pipe( plugins.size( { title: 'Images: Theme' } ) )
		           .pipe( gulp.dest( paths.dist.images ) );
	};
};
