module.exports = function( gulp, plugins, paths ) {
	return function() {
		return plugins.del(
			[
				'./wp-content/themes/customtheme',
				paths.src.root + '/.gitignore',
				paths.src.root + '/README.md'
			]
		);
	};
};
